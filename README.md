# Component Placement


## Licence and credits

Plugin code licensed under MIT, see `LICENSE` for more info.

KiCad Plugin code/structure from [Interactive HTML BOM](https://github.com/openscopeproject/InteractiveHtmlBom) and [KiBuzzard](https://github.com/gregdavill/KiBuzzard)

## Install wxFormBuilder
- Download wxFormBuilder https://github.com/wxFormBuilder/wxFormBuilder/releases
- Install flatpak `sudo apt install flatpak`
- Install wxFormBuilder `sudo flatpak install wxFormBuilder-3.10.1-x86_64.flatpak`
- Run wxFormBuilder `flatpak run org.wxformbuilder.wxFormBuilder`

## Run dialog_test
- `python3 dialog_test.py`